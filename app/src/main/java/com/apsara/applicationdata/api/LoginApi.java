package com.apsara.applicationdata.api;

import com.apsara.applicationdata.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginApi {
    @GET("api/user")
    Call<LoginResponse> login(@Query("userId") String userId, @Query("password") String password);
}
