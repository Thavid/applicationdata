package com.apsara.applicationdata;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.apsara.applicationdata.api.LoginApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText edUserName;
    private EditText edPassword;
    private Button btnLogin;
    private LoginApi api;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edUserName = findViewById(R.id.ed_username);
        edPassword = findViewById(R.id.ed_password);
        btnLogin = findViewById(R.id.btn_login);

        final Intent intent = new Intent(this,MainActivity.class);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edUserName.getText().toString().equals("") && !edPassword.getText().toString().equals("")){
                    api = ServiceGenerator.createService(LoginApi.class);
                    Call<LoginResponse> call = api.login(edUserName.getText().toString(),edPassword.getText().toString());
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if(response.isSuccessful()){
                                startActivity(intent);
                            }else {
                                Log.e("oooooooooooooooo",response.body().message);
                                intent.putExtra("NAME",response.body().data.var_name);
                                Toast.makeText(LoginActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            Log.e("ooooooooooooooo",t.getMessage());
                            Toast.makeText(LoginActivity.this, "Can't connect to server", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else {
                    Toast.makeText(LoginActivity.this, "Please input username and password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
