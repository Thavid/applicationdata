package com.apsara.applicationdata;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("message")
    public String message;
    @SerializedName("status")
    public String status;
    @SerializedName("statusCode")
    public int statusCode;
    @SerializedName("data")
    public Data data;

    @Override
    public String toString() {
        return "LoginResponse{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", statusCode=" + statusCode +
                ", data=" + data +
                '}';
    }

    public static class Data {
        @SerializedName("var_id")
        public String var_id;
        @SerializedName("var_pwd")
        public String var_pwd;
        @SerializedName("var_name")
        public String var_name;

        @Override
        public String toString() {
            return "Data{" +
                    "var_id='" + var_id + '\'' +
                    ", var_pwd='" + var_pwd + '\'' +
                    ", var_name='" + var_name + '\'' +
                    '}';
        }
    }
}
